#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <string>
#include <list>
#include <map>
#include <deque>

#include <stdlib.h>
#include <gio/gio.h>

#include "dbus/DBusBindingGio.hh"
#include "dbus/DBusException.hh"
#include "DBusWorkrave.hh"

using namespace std;
using namespace workrave::dbus;

class DBusWorkrave_Marshall : public DBusMarshallGio
{
public:
  void get_break_id(GVariant *variant, BreakId *result);
  GVariant *put_break_id(const BreakId *result);
  void get_operation_mode(GVariant *variant, OperationMode *result);
  GVariant *put_operation_mode(const OperationMode *result);
  void get_usage_mode(GVariant *variant, UsageMode *result);
  GVariant *put_usage_mode(const UsageMode *result);



};


void
DBusWorkrave_Marshall::get_break_id(GVariant *variant, BreakId *result)
{
  std::string value;
  get_string(variant, &value);

if("microbreak" == value)
    {
      *result = BREAK_ID_MICRO_BREAK;
    }
else if("restbreak" == value)
    {
      *result = BREAK_ID_REST_BREAK;
    }
else if("dailylimit" == value)
    {
      *result = BREAK_ID_DAILY_LIMIT;
    }
  else
    {
      throw DBusRemoteException()
        << message_info("Type error in enum")
        << error_code_info(DBUS_ERROR_INVALID_ARGS)
        << actual_type_info("break_id");
    }
}

GVariant *
DBusWorkrave_Marshall::put_break_id(const BreakId *result)
{
  string value;
  switch (*result)
    {
    case BREAK_ID_MICRO_BREAK:
      value = "microbreak";
      break;
    case BREAK_ID_REST_BREAK:
      value = "restbreak";
      break;
    case BREAK_ID_DAILY_LIMIT:
      value = "dailylimit";
      break;
    default:
      throw DBusRemoteException()
        << message_info("Type error in enum")
        << error_code_info(DBUS_ERROR_INVALID_ARGS)
        << actual_type_info("break_id");
    }

  return put_string(&value);
}


void
DBusWorkrave_Marshall::get_operation_mode(GVariant *variant, OperationMode *result)
{
  std::string value;
  get_string(variant, &value);

if("normal" == value)
    {
      *result = OPERATION_MODE_NORMAL;
    }
else if("suspended" == value)
    {
      *result = OPERATION_MODE_SUSPENDED;
    }
else if("quiet" == value)
    {
      *result = OPERATION_MODE_QUIET;
    }
  else
    {
      throw DBusRemoteException()
        << message_info("Type error in enum")
        << error_code_info(DBUS_ERROR_INVALID_ARGS)
        << actual_type_info("operation_mode");
    }
}

GVariant *
DBusWorkrave_Marshall::put_operation_mode(const OperationMode *result)
{
  string value;
  switch (*result)
    {
    case OPERATION_MODE_NORMAL:
      value = "normal";
      break;
    case OPERATION_MODE_SUSPENDED:
      value = "suspended";
      break;
    case OPERATION_MODE_QUIET:
      value = "quiet";
      break;
    default:
      throw DBusRemoteException()
        << message_info("Type error in enum")
        << error_code_info(DBUS_ERROR_INVALID_ARGS)
        << actual_type_info("operation_mode");
    }

  return put_string(&value);
}


void
DBusWorkrave_Marshall::get_usage_mode(GVariant *variant, UsageMode *result)
{
  std::string value;
  get_string(variant, &value);

if("normal" == value)
    {
      *result = USAGE_MODE_NORMAL;
    }
else if("reading" == value)
    {
      *result = USAGE_MODE_READING;
    }
  else
    {
      throw DBusRemoteException()
        << message_info("Type error in enum")
        << error_code_info(DBUS_ERROR_INVALID_ARGS)
        << actual_type_info("usage_mode");
    }
}

GVariant *
DBusWorkrave_Marshall::put_usage_mode(const UsageMode *result)
{
  string value;
  switch (*result)
    {
    case USAGE_MODE_NORMAL:
      value = "normal";
      break;
    case USAGE_MODE_READING:
      value = "reading";
      break;
    default:
      throw DBusRemoteException()
        << message_info("Type error in enum")
        << error_code_info(DBUS_ERROR_INVALID_ARGS)
        << actual_type_info("usage_mode");
    }

  return put_string(&value);
}







class org_workrave_CoreInterface_Stub : public DBusBindingGio, public org_workrave_CoreInterface, DBusWorkrave_Marshall
{
private:
  typedef void (org_workrave_CoreInterface_Stub::*DBusMethodPointer)(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs);

  struct DBusMethod
  {
    const string name;
    DBusMethodPointer fn;
  };

  virtual void call(const std::string &method_name, void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs);

  virtual const char *get_interface_introspect()
  {
    return interface_introspect;
  }

public:
  org_workrave_CoreInterface_Stub(IDBus::Ptr dbus);
  ~org_workrave_CoreInterface_Stub();

  void MicrobreakChanged(const string &path
      , std::string progress
  );
  void RestbreakChanged(const string &path
      , std::string progress
  );
  void DailylimitChanged(const string &path
      , std::string progress
  );
  void OperationModeChanged(const string &path
      , OperationMode mode
  );
  void UsageModeChanged(const string &path
      , UsageMode mode
  );
  void BreakPostponed(const string &path
      , BreakId timer_id
  );
  void BreakSkipped(const string &path
      , BreakId timer_id
  );

private:
  void SetOperationMode(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs);
  void GetOperationMode(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs);
  void SetUsageMode(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs);
  void GetUsageMode(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs);
  void ReportActivity(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs);
  void IsTimerRunning(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs);
  void GetTimerIdle(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs);
  void GetTimerElapsed(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs);
  void GetTimerRemaining(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs);
  void GetTimerOverdue(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs);
  void GetTime(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs);
  void GetBreakState(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs);
  void IsActive(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs);
  void PostponeBreak(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs);
  void SkipBreak(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs);

  static const DBusMethod method_table[];
  static const char *interface_introspect;
};


org_workrave_CoreInterface *org_workrave_CoreInterface::instance(const ::workrave::dbus::IDBus::Ptr dbus)
{
  org_workrave_CoreInterface_Stub *iface = NULL;
  DBusBinding *binding = dbus->find_binding("org.workrave.CoreInterface");

  if (binding != NULL)
    {
      iface = dynamic_cast<org_workrave_CoreInterface_Stub *>(binding);
    }

  return iface;
}

org_workrave_CoreInterface_Stub::org_workrave_CoreInterface_Stub(IDBus::Ptr dbus)
  : DBusBindingGio(dbus)
{
}

org_workrave_CoreInterface_Stub::~org_workrave_CoreInterface_Stub()
{
}

void
org_workrave_CoreInterface_Stub::call(const std::string &method_name, void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs)
{
  const DBusMethod *table = method_table;
  while (table->fn != NULL)
    {
      if (method_name == table->name)
        {
          DBusMethodPointer ptr = table->fn;
          if (ptr != NULL)
            {
              (this->*ptr)(object, invocation, sender, inargs);
            }
          return;
        }
      table++;
    }

  throw DBusRemoteException()
    << message_info("Unknown method")
    << error_code_info(DBUS_ERROR_UNKNOWN_METHOD)
    << method_info(method_name)
    << interface_info("org.workrave.CoreInterface");
}


void
org_workrave_CoreInterface_Stub::SetOperationMode(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs)
{
  (void) sender;

  try
    {
      Core *dbus_object = (Core *) object;

      OperationMode p_mode
      ;

      gsize num_in_args = g_variant_n_children(inargs);
      if (num_in_args != 1)
        {
          throw DBusRemoteException()
            << message_info("Incorrecy number of in-paraeters")
            << error_code_info(DBUS_ERROR_INVALID_ARGS)
            << method_info("SetOperationMode")
            << interface_info("org.workrave.CoreInterface");
        }

      GVariant *v_mode = g_variant_get_child_value(inargs, 0 );
      get_operation_mode(v_mode, &p_mode);

      dbus_object->set_operation_mode(

       p_mode
      );

      GVariant *out = NULL;

      g_dbus_method_invocation_return_value(invocation, out);
    }
  catch (const DBusRemoteException &e)
    {
      e << method_info("SetOperationMode")
        << interface_info("org.workrave.CoreInterface");
      throw;
    }

}

void
org_workrave_CoreInterface_Stub::GetOperationMode(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs)
{
  (void) sender;

  try
    {
      Core *dbus_object = (Core *) object;

      OperationMode p_mode
      ;

      gsize num_in_args = g_variant_n_children(inargs);
      if (num_in_args != 0)
        {
          throw DBusRemoteException()
            << message_info("Incorrecy number of in-paraeters")
            << error_code_info(DBUS_ERROR_INVALID_ARGS)
            << method_info("GetOperationMode")
            << interface_info("org.workrave.CoreInterface");
        }


      p_mode = dbus_object->get_operation_mode(

      );

      GVariantBuilder builder;
      g_variant_builder_init(&builder, (GVariantType*)"(s)");

      GVariant *v_mode = put_operation_mode(&p_mode);
      g_variant_builder_add_value(&builder, v_mode);

      GVariant *out = g_variant_builder_end(&builder);

      g_dbus_method_invocation_return_value(invocation, out);
    }
  catch (const DBusRemoteException &e)
    {
      e << method_info("GetOperationMode")
        << interface_info("org.workrave.CoreInterface");
      throw;
    }

}

void
org_workrave_CoreInterface_Stub::SetUsageMode(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs)
{
  (void) sender;

  try
    {
      Core *dbus_object = (Core *) object;

      UsageMode p_mode
      ;

      gsize num_in_args = g_variant_n_children(inargs);
      if (num_in_args != 1)
        {
          throw DBusRemoteException()
            << message_info("Incorrecy number of in-paraeters")
            << error_code_info(DBUS_ERROR_INVALID_ARGS)
            << method_info("SetUsageMode")
            << interface_info("org.workrave.CoreInterface");
        }

      GVariant *v_mode = g_variant_get_child_value(inargs, 0 );
      get_usage_mode(v_mode, &p_mode);

      dbus_object->set_usage_mode(

       p_mode
      );

      GVariant *out = NULL;

      g_dbus_method_invocation_return_value(invocation, out);
    }
  catch (const DBusRemoteException &e)
    {
      e << method_info("SetUsageMode")
        << interface_info("org.workrave.CoreInterface");
      throw;
    }

}

void
org_workrave_CoreInterface_Stub::GetUsageMode(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs)
{
  (void) sender;

  try
    {
      Core *dbus_object = (Core *) object;

      UsageMode p_mode
      ;

      gsize num_in_args = g_variant_n_children(inargs);
      if (num_in_args != 0)
        {
          throw DBusRemoteException()
            << message_info("Incorrecy number of in-paraeters")
            << error_code_info(DBUS_ERROR_INVALID_ARGS)
            << method_info("GetUsageMode")
            << interface_info("org.workrave.CoreInterface");
        }


      p_mode = dbus_object->get_usage_mode(

      );

      GVariantBuilder builder;
      g_variant_builder_init(&builder, (GVariantType*)"(s)");

      GVariant *v_mode = put_usage_mode(&p_mode);
      g_variant_builder_add_value(&builder, v_mode);

      GVariant *out = g_variant_builder_end(&builder);

      g_dbus_method_invocation_return_value(invocation, out);
    }
  catch (const DBusRemoteException &e)
    {
      e << method_info("GetUsageMode")
        << interface_info("org.workrave.CoreInterface");
      throw;
    }

}

void
org_workrave_CoreInterface_Stub::ReportActivity(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs)
{
  (void) sender;

  try
    {
      Core *dbus_object = (Core *) object;

      std::string p_who
      ;
      bool p_act
      ;

      gsize num_in_args = g_variant_n_children(inargs);
      if (num_in_args != 2)
        {
          throw DBusRemoteException()
            << message_info("Incorrecy number of in-paraeters")
            << error_code_info(DBUS_ERROR_INVALID_ARGS)
            << method_info("ReportActivity")
            << interface_info("org.workrave.CoreInterface");
        }

      GVariant *v_who = g_variant_get_child_value(inargs, 0 );
      get_string(v_who, &p_who);
      GVariant *v_act = g_variant_get_child_value(inargs, 1 );
      get_bool(v_act, &p_act);

      dbus_object->report_external_activity(

       p_who
      , p_act
      );

      GVariant *out = NULL;

      g_dbus_method_invocation_return_value(invocation, out);
    }
  catch (const DBusRemoteException &e)
    {
      e << method_info("ReportActivity")
        << interface_info("org.workrave.CoreInterface");
      throw;
    }

}

void
org_workrave_CoreInterface_Stub::IsTimerRunning(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs)
{
  (void) sender;

  try
    {
      Core *dbus_object = (Core *) object;

      BreakId p_timer_id
      ;
      bool p_value
      ;

      gsize num_in_args = g_variant_n_children(inargs);
      if (num_in_args != 1)
        {
          throw DBusRemoteException()
            << message_info("Incorrecy number of in-paraeters")
            << error_code_info(DBUS_ERROR_INVALID_ARGS)
            << method_info("IsTimerRunning")
            << interface_info("org.workrave.CoreInterface");
        }

      GVariant *v_timer_id = g_variant_get_child_value(inargs, 0 );
      get_break_id(v_timer_id, &p_timer_id);

      dbus_object->is_timer_running(

       p_timer_id
      , p_value
      );

      GVariantBuilder builder;
      g_variant_builder_init(&builder, (GVariantType*)"(b)");

      GVariant *v_value = put_bool(&p_value);
      g_variant_builder_add_value(&builder, v_value);

      GVariant *out = g_variant_builder_end(&builder);

      g_dbus_method_invocation_return_value(invocation, out);
    }
  catch (const DBusRemoteException &e)
    {
      e << method_info("IsTimerRunning")
        << interface_info("org.workrave.CoreInterface");
      throw;
    }

}

void
org_workrave_CoreInterface_Stub::GetTimerIdle(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs)
{
  (void) sender;

  try
    {
      Core *dbus_object = (Core *) object;

      BreakId p_timer_id
      ;
      int32_t p_value
      ;

      gsize num_in_args = g_variant_n_children(inargs);
      if (num_in_args != 1)
        {
          throw DBusRemoteException()
            << message_info("Incorrecy number of in-paraeters")
            << error_code_info(DBUS_ERROR_INVALID_ARGS)
            << method_info("GetTimerIdle")
            << interface_info("org.workrave.CoreInterface");
        }

      GVariant *v_timer_id = g_variant_get_child_value(inargs, 0 );
      get_break_id(v_timer_id, &p_timer_id);

      dbus_object->get_timer_idle(

       p_timer_id
      , &p_value
      );

      GVariantBuilder builder;
      g_variant_builder_init(&builder, (GVariantType*)"(i)");

      GVariant *v_value = put_int32(&p_value);
      g_variant_builder_add_value(&builder, v_value);

      GVariant *out = g_variant_builder_end(&builder);

      g_dbus_method_invocation_return_value(invocation, out);
    }
  catch (const DBusRemoteException &e)
    {
      e << method_info("GetTimerIdle")
        << interface_info("org.workrave.CoreInterface");
      throw;
    }

}

void
org_workrave_CoreInterface_Stub::GetTimerElapsed(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs)
{
  (void) sender;

  try
    {
      Core *dbus_object = (Core *) object;

      BreakId p_timer_id
      ;
      int32_t p_value
      ;

      gsize num_in_args = g_variant_n_children(inargs);
      if (num_in_args != 1)
        {
          throw DBusRemoteException()
            << message_info("Incorrecy number of in-paraeters")
            << error_code_info(DBUS_ERROR_INVALID_ARGS)
            << method_info("GetTimerElapsed")
            << interface_info("org.workrave.CoreInterface");
        }

      GVariant *v_timer_id = g_variant_get_child_value(inargs, 0 );
      get_break_id(v_timer_id, &p_timer_id);

      dbus_object->get_timer_elapsed(

       p_timer_id
      , &p_value
      );

      GVariantBuilder builder;
      g_variant_builder_init(&builder, (GVariantType*)"(i)");

      GVariant *v_value = put_int32(&p_value);
      g_variant_builder_add_value(&builder, v_value);

      GVariant *out = g_variant_builder_end(&builder);

      g_dbus_method_invocation_return_value(invocation, out);
    }
  catch (const DBusRemoteException &e)
    {
      e << method_info("GetTimerElapsed")
        << interface_info("org.workrave.CoreInterface");
      throw;
    }

}

void
org_workrave_CoreInterface_Stub::GetTimerRemaining(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs)
{
  (void) sender;

  try
    {
      Core *dbus_object = (Core *) object;

      BreakId p_timer_id
      ;
      int32_t p_value
      ;

      gsize num_in_args = g_variant_n_children(inargs);
      if (num_in_args != 1)
        {
          throw DBusRemoteException()
            << message_info("Incorrecy number of in-paraeters")
            << error_code_info(DBUS_ERROR_INVALID_ARGS)
            << method_info("GetTimerRemaining")
            << interface_info("org.workrave.CoreInterface");
        }

      GVariant *v_timer_id = g_variant_get_child_value(inargs, 0 );
      get_break_id(v_timer_id, &p_timer_id);

      dbus_object->get_timer_remaining(

       p_timer_id
      , &p_value
      );

      GVariantBuilder builder;
      g_variant_builder_init(&builder, (GVariantType*)"(i)");

      GVariant *v_value = put_int32(&p_value);
      g_variant_builder_add_value(&builder, v_value);

      GVariant *out = g_variant_builder_end(&builder);

      g_dbus_method_invocation_return_value(invocation, out);
    }
  catch (const DBusRemoteException &e)
    {
      e << method_info("GetTimerRemaining")
        << interface_info("org.workrave.CoreInterface");
      throw;
    }

}

void
org_workrave_CoreInterface_Stub::GetTimerOverdue(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs)
{
  (void) sender;

  try
    {
      Core *dbus_object = (Core *) object;

      BreakId p_timer_id
      ;
      int32_t p_value
      ;

      gsize num_in_args = g_variant_n_children(inargs);
      if (num_in_args != 1)
        {
          throw DBusRemoteException()
            << message_info("Incorrecy number of in-paraeters")
            << error_code_info(DBUS_ERROR_INVALID_ARGS)
            << method_info("GetTimerOverdue")
            << interface_info("org.workrave.CoreInterface");
        }

      GVariant *v_timer_id = g_variant_get_child_value(inargs, 0 );
      get_break_id(v_timer_id, &p_timer_id);

      dbus_object->get_timer_overdue(

       p_timer_id
      , &p_value
      );

      GVariantBuilder builder;
      g_variant_builder_init(&builder, (GVariantType*)"(i)");

      GVariant *v_value = put_int32(&p_value);
      g_variant_builder_add_value(&builder, v_value);

      GVariant *out = g_variant_builder_end(&builder);

      g_dbus_method_invocation_return_value(invocation, out);
    }
  catch (const DBusRemoteException &e)
    {
      e << method_info("GetTimerOverdue")
        << interface_info("org.workrave.CoreInterface");
      throw;
    }

}

void
org_workrave_CoreInterface_Stub::GetTime(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs)
{
  (void) sender;

  try
    {
      Core *dbus_object = (Core *) object;

      int32_t p_value
      ;

      gsize num_in_args = g_variant_n_children(inargs);
      if (num_in_args != 0)
        {
          throw DBusRemoteException()
            << message_info("Incorrecy number of in-paraeters")
            << error_code_info(DBUS_ERROR_INVALID_ARGS)
            << method_info("GetTime")
            << interface_info("org.workrave.CoreInterface");
        }


      p_value = dbus_object->get_time(

      );

      GVariantBuilder builder;
      g_variant_builder_init(&builder, (GVariantType*)"(i)");

      GVariant *v_value = put_int32(&p_value);
      g_variant_builder_add_value(&builder, v_value);

      GVariant *out = g_variant_builder_end(&builder);

      g_dbus_method_invocation_return_value(invocation, out);
    }
  catch (const DBusRemoteException &e)
    {
      e << method_info("GetTime")
        << interface_info("org.workrave.CoreInterface");
      throw;
    }

}

void
org_workrave_CoreInterface_Stub::GetBreakState(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs)
{
  (void) sender;

  try
    {
      Core *dbus_object = (Core *) object;

      BreakId p_timer_id
      ;
      std::string p_stage
      ;

      gsize num_in_args = g_variant_n_children(inargs);
      if (num_in_args != 1)
        {
          throw DBusRemoteException()
            << message_info("Incorrecy number of in-paraeters")
            << error_code_info(DBUS_ERROR_INVALID_ARGS)
            << method_info("GetBreakState")
            << interface_info("org.workrave.CoreInterface");
        }

      GVariant *v_timer_id = g_variant_get_child_value(inargs, 0 );
      get_break_id(v_timer_id, &p_timer_id);

      p_stage = dbus_object->get_break_stage(

       p_timer_id
      );

      GVariantBuilder builder;
      g_variant_builder_init(&builder, (GVariantType*)"(s)");

      GVariant *v_stage = put_string(&p_stage);
      g_variant_builder_add_value(&builder, v_stage);

      GVariant *out = g_variant_builder_end(&builder);

      g_dbus_method_invocation_return_value(invocation, out);
    }
  catch (const DBusRemoteException &e)
    {
      e << method_info("GetBreakState")
        << interface_info("org.workrave.CoreInterface");
      throw;
    }

}

void
org_workrave_CoreInterface_Stub::IsActive(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs)
{
  (void) sender;

  try
    {
      Core *dbus_object = (Core *) object;

      bool p_value
      ;

      gsize num_in_args = g_variant_n_children(inargs);
      if (num_in_args != 0)
        {
          throw DBusRemoteException()
            << message_info("Incorrecy number of in-paraeters")
            << error_code_info(DBUS_ERROR_INVALID_ARGS)
            << method_info("IsActive")
            << interface_info("org.workrave.CoreInterface");
        }


      p_value = dbus_object->is_user_active(

      );

      GVariantBuilder builder;
      g_variant_builder_init(&builder, (GVariantType*)"(b)");

      GVariant *v_value = put_bool(&p_value);
      g_variant_builder_add_value(&builder, v_value);

      GVariant *out = g_variant_builder_end(&builder);

      g_dbus_method_invocation_return_value(invocation, out);
    }
  catch (const DBusRemoteException &e)
    {
      e << method_info("IsActive")
        << interface_info("org.workrave.CoreInterface");
      throw;
    }

}

void
org_workrave_CoreInterface_Stub::PostponeBreak(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs)
{
  (void) sender;

  try
    {
      Core *dbus_object = (Core *) object;

      BreakId p_timer_id
      ;

      gsize num_in_args = g_variant_n_children(inargs);
      if (num_in_args != 1)
        {
          throw DBusRemoteException()
            << message_info("Incorrecy number of in-paraeters")
            << error_code_info(DBUS_ERROR_INVALID_ARGS)
            << method_info("PostponeBreak")
            << interface_info("org.workrave.CoreInterface");
        }

      GVariant *v_timer_id = g_variant_get_child_value(inargs, 0 );
      get_break_id(v_timer_id, &p_timer_id);

      dbus_object->postpone_break(

       p_timer_id
      );

      GVariant *out = NULL;

      g_dbus_method_invocation_return_value(invocation, out);
    }
  catch (const DBusRemoteException &e)
    {
      e << method_info("PostponeBreak")
        << interface_info("org.workrave.CoreInterface");
      throw;
    }

}

void
org_workrave_CoreInterface_Stub::SkipBreak(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs)
{
  (void) sender;

  try
    {
      Core *dbus_object = (Core *) object;

      BreakId p_timer_id
      ;

      gsize num_in_args = g_variant_n_children(inargs);
      if (num_in_args != 1)
        {
          throw DBusRemoteException()
            << message_info("Incorrecy number of in-paraeters")
            << error_code_info(DBUS_ERROR_INVALID_ARGS)
            << method_info("SkipBreak")
            << interface_info("org.workrave.CoreInterface");
        }

      GVariant *v_timer_id = g_variant_get_child_value(inargs, 0 );
      get_break_id(v_timer_id, &p_timer_id);

      dbus_object->skip_break(

       p_timer_id
      );

      GVariant *out = NULL;

      g_dbus_method_invocation_return_value(invocation, out);
    }
  catch (const DBusRemoteException &e)
    {
      e << method_info("SkipBreak")
        << interface_info("org.workrave.CoreInterface");
      throw;
    }

}

void org_workrave_CoreInterface_Stub::MicrobreakChanged(const string &path
      , std::string progress
)
{
  IDBusPrivateGio::Ptr p = std::dynamic_pointer_cast<IDBusPrivateGio>(dbus);

  GDBusConnection *connection = p->get_connection();
  if (connection == NULL)
    {
      return;
    }

  GVariantBuilder builder;
  g_variant_builder_init(&builder, (GVariantType*)"(s)");

  GVariant *v_progress = put_string(&progress);
  g_variant_builder_add_value(&builder, v_progress);

  GVariant *out = g_variant_builder_end(&builder);

  GError *error = NULL;
  g_dbus_connection_emit_signal(connection,
                                NULL,
                                path.c_str(),
                                "org.workrave.CoreInterface",
                                "MicrobreakChanged",
                                out,
                                &error);

  if (error != NULL)
    {
      g_error_free(error);
    }
}
void org_workrave_CoreInterface_Stub::RestbreakChanged(const string &path
      , std::string progress
)
{
  IDBusPrivateGio::Ptr p = std::dynamic_pointer_cast<IDBusPrivateGio>(dbus);

  GDBusConnection *connection = p->get_connection();
  if (connection == NULL)
    {
      return;
    }

  GVariantBuilder builder;
  g_variant_builder_init(&builder, (GVariantType*)"(s)");

  GVariant *v_progress = put_string(&progress);
  g_variant_builder_add_value(&builder, v_progress);

  GVariant *out = g_variant_builder_end(&builder);

  GError *error = NULL;
  g_dbus_connection_emit_signal(connection,
                                NULL,
                                path.c_str(),
                                "org.workrave.CoreInterface",
                                "RestbreakChanged",
                                out,
                                &error);

  if (error != NULL)
    {
      g_error_free(error);
    }
}
void org_workrave_CoreInterface_Stub::DailylimitChanged(const string &path
      , std::string progress
)
{
  IDBusPrivateGio::Ptr p = std::dynamic_pointer_cast<IDBusPrivateGio>(dbus);

  GDBusConnection *connection = p->get_connection();
  if (connection == NULL)
    {
      return;
    }

  GVariantBuilder builder;
  g_variant_builder_init(&builder, (GVariantType*)"(s)");

  GVariant *v_progress = put_string(&progress);
  g_variant_builder_add_value(&builder, v_progress);

  GVariant *out = g_variant_builder_end(&builder);

  GError *error = NULL;
  g_dbus_connection_emit_signal(connection,
                                NULL,
                                path.c_str(),
                                "org.workrave.CoreInterface",
                                "DailylimitChanged",
                                out,
                                &error);

  if (error != NULL)
    {
      g_error_free(error);
    }
}
void org_workrave_CoreInterface_Stub::OperationModeChanged(const string &path
      , OperationMode mode
)
{
  IDBusPrivateGio::Ptr p = std::dynamic_pointer_cast<IDBusPrivateGio>(dbus);

  GDBusConnection *connection = p->get_connection();
  if (connection == NULL)
    {
      return;
    }

  GVariantBuilder builder;
  g_variant_builder_init(&builder, (GVariantType*)"(s)");

  GVariant *v_mode = put_operation_mode(&mode);
  g_variant_builder_add_value(&builder, v_mode);

  GVariant *out = g_variant_builder_end(&builder);

  GError *error = NULL;
  g_dbus_connection_emit_signal(connection,
                                NULL,
                                path.c_str(),
                                "org.workrave.CoreInterface",
                                "OperationModeChanged",
                                out,
                                &error);

  if (error != NULL)
    {
      g_error_free(error);
    }
}
void org_workrave_CoreInterface_Stub::UsageModeChanged(const string &path
      , UsageMode mode
)
{
  IDBusPrivateGio::Ptr p = std::dynamic_pointer_cast<IDBusPrivateGio>(dbus);

  GDBusConnection *connection = p->get_connection();
  if (connection == NULL)
    {
      return;
    }

  GVariantBuilder builder;
  g_variant_builder_init(&builder, (GVariantType*)"(s)");

  GVariant *v_mode = put_usage_mode(&mode);
  g_variant_builder_add_value(&builder, v_mode);

  GVariant *out = g_variant_builder_end(&builder);

  GError *error = NULL;
  g_dbus_connection_emit_signal(connection,
                                NULL,
                                path.c_str(),
                                "org.workrave.CoreInterface",
                                "UsageModeChanged",
                                out,
                                &error);

  if (error != NULL)
    {
      g_error_free(error);
    }
}
void org_workrave_CoreInterface_Stub::BreakPostponed(const string &path
      , BreakId timer_id
)
{
  IDBusPrivateGio::Ptr p = std::dynamic_pointer_cast<IDBusPrivateGio>(dbus);

  GDBusConnection *connection = p->get_connection();
  if (connection == NULL)
    {
      return;
    }

  GVariantBuilder builder;
  g_variant_builder_init(&builder, (GVariantType*)"(s)");

  GVariant *v_timer_id = put_break_id(&timer_id);
  g_variant_builder_add_value(&builder, v_timer_id);

  GVariant *out = g_variant_builder_end(&builder);

  GError *error = NULL;
  g_dbus_connection_emit_signal(connection,
                                NULL,
                                path.c_str(),
                                "org.workrave.CoreInterface",
                                "BreakPostponed",
                                out,
                                &error);

  if (error != NULL)
    {
      g_error_free(error);
    }
}
void org_workrave_CoreInterface_Stub::BreakSkipped(const string &path
      , BreakId timer_id
)
{
  IDBusPrivateGio::Ptr p = std::dynamic_pointer_cast<IDBusPrivateGio>(dbus);

  GDBusConnection *connection = p->get_connection();
  if (connection == NULL)
    {
      return;
    }

  GVariantBuilder builder;
  g_variant_builder_init(&builder, (GVariantType*)"(s)");

  GVariant *v_timer_id = put_break_id(&timer_id);
  g_variant_builder_add_value(&builder, v_timer_id);

  GVariant *out = g_variant_builder_end(&builder);

  GError *error = NULL;
  g_dbus_connection_emit_signal(connection,
                                NULL,
                                path.c_str(),
                                "org.workrave.CoreInterface",
                                "BreakSkipped",
                                out,
                                &error);

  if (error != NULL)
    {
      g_error_free(error);
    }
}

const org_workrave_CoreInterface_Stub::DBusMethod org_workrave_CoreInterface_Stub::method_table[] = {
  { "SetOperationMode", &org_workrave_CoreInterface_Stub::SetOperationMode },
  { "GetOperationMode", &org_workrave_CoreInterface_Stub::GetOperationMode },
  { "SetUsageMode", &org_workrave_CoreInterface_Stub::SetUsageMode },
  { "GetUsageMode", &org_workrave_CoreInterface_Stub::GetUsageMode },
  { "ReportActivity", &org_workrave_CoreInterface_Stub::ReportActivity },
  { "IsTimerRunning", &org_workrave_CoreInterface_Stub::IsTimerRunning },
  { "GetTimerIdle", &org_workrave_CoreInterface_Stub::GetTimerIdle },
  { "GetTimerElapsed", &org_workrave_CoreInterface_Stub::GetTimerElapsed },
  { "GetTimerRemaining", &org_workrave_CoreInterface_Stub::GetTimerRemaining },
  { "GetTimerOverdue", &org_workrave_CoreInterface_Stub::GetTimerOverdue },
  { "GetTime", &org_workrave_CoreInterface_Stub::GetTime },
  { "GetBreakState", &org_workrave_CoreInterface_Stub::GetBreakState },
  { "IsActive", &org_workrave_CoreInterface_Stub::IsActive },
  { "PostponeBreak", &org_workrave_CoreInterface_Stub::PostponeBreak },
  { "SkipBreak", &org_workrave_CoreInterface_Stub::SkipBreak },
  { "", NULL }
};

const char *
org_workrave_CoreInterface_Stub::interface_introspect =
  "  <interface name=\"org.workrave.CoreInterface\">\n"
  "    <method name=\"SetOperationMode\">\n"
  "      <arg type=\"s\" name=\"mode\" direction=\"in\" />\n"
  "    </method>\n"
  "    <method name=\"GetOperationMode\">\n"
  "      <arg type=\"s\" name=\"mode\" direction=\"out\" />\n"
  "    </method>\n"
  "    <method name=\"SetUsageMode\">\n"
  "      <arg type=\"s\" name=\"mode\" direction=\"in\" />\n"
  "    </method>\n"
  "    <method name=\"GetUsageMode\">\n"
  "      <arg type=\"s\" name=\"mode\" direction=\"out\" />\n"
  "    </method>\n"
  "    <method name=\"ReportActivity\">\n"
  "      <arg type=\"s\" name=\"who\" direction=\"in\" />\n"
  "      <arg type=\"b\" name=\"act\" direction=\"in\" />\n"
  "    </method>\n"
  "    <method name=\"IsTimerRunning\">\n"
  "      <arg type=\"s\" name=\"timer_id\" direction=\"in\" />\n"
  "      <arg type=\"b\" name=\"value\" direction=\"out\" />\n"
  "    </method>\n"
  "    <method name=\"GetTimerIdle\">\n"
  "      <arg type=\"s\" name=\"timer_id\" direction=\"in\" />\n"
  "      <arg type=\"i\" name=\"value\" direction=\"out\" />\n"
  "    </method>\n"
  "    <method name=\"GetTimerElapsed\">\n"
  "      <arg type=\"s\" name=\"timer_id\" direction=\"in\" />\n"
  "      <arg type=\"i\" name=\"value\" direction=\"out\" />\n"
  "    </method>\n"
  "    <method name=\"GetTimerRemaining\">\n"
  "      <arg type=\"s\" name=\"timer_id\" direction=\"in\" />\n"
  "      <arg type=\"i\" name=\"value\" direction=\"out\" />\n"
  "    </method>\n"
  "    <method name=\"GetTimerOverdue\">\n"
  "      <arg type=\"s\" name=\"timer_id\" direction=\"in\" />\n"
  "      <arg type=\"i\" name=\"value\" direction=\"out\" />\n"
  "    </method>\n"
  "    <method name=\"GetTime\">\n"
  "      <arg type=\"i\" name=\"value\" direction=\"out\" />\n"
  "    </method>\n"
  "    <method name=\"GetBreakState\">\n"
  "      <arg type=\"s\" name=\"timer_id\" direction=\"in\" />\n"
  "      <arg type=\"s\" name=\"stage\" direction=\"out\" />\n"
  "    </method>\n"
  "    <method name=\"IsActive\">\n"
  "      <arg type=\"b\" name=\"value\" direction=\"out\" />\n"
  "    </method>\n"
  "    <method name=\"PostponeBreak\">\n"
  "      <arg type=\"s\" name=\"timer_id\" direction=\"in\" />\n"
  "    </method>\n"
  "    <method name=\"SkipBreak\">\n"
  "      <arg type=\"s\" name=\"timer_id\" direction=\"in\" />\n"
  "    </method>\n"
  "    <signal name=\"MicrobreakChanged\">\n"
  "      <arg type=\"s\" name=\"progress\" />\n"
  "    </signal>\n"
  "    <signal name=\"RestbreakChanged\">\n"
  "      <arg type=\"s\" name=\"progress\" />\n"
  "    </signal>\n"
  "    <signal name=\"DailylimitChanged\">\n"
  "      <arg type=\"s\" name=\"progress\" />\n"
  "    </signal>\n"
  "    <signal name=\"OperationModeChanged\">\n"
  "      <arg type=\"s\" name=\"mode\" />\n"
  "    </signal>\n"
  "    <signal name=\"UsageModeChanged\">\n"
  "      <arg type=\"s\" name=\"mode\" />\n"
  "    </signal>\n"
  "    <signal name=\"BreakPostponed\">\n"
  "      <arg type=\"s\" name=\"timer_id\" />\n"
  "    </signal>\n"
  "    <signal name=\"BreakSkipped\">\n"
  "      <arg type=\"s\" name=\"timer_id\" />\n"
  "    </signal>\n"
  "  </interface>\n";



#if defined(HAVE_TESTS)


class org_workrave_DebugInterface_Stub : public DBusBindingGio, public org_workrave_DebugInterface, DBusWorkrave_Marshall
{
private:
  typedef void (org_workrave_DebugInterface_Stub::*DBusMethodPointer)(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs);

  struct DBusMethod
  {
    const string name;
    DBusMethodPointer fn;
  };

  virtual void call(const std::string &method_name, void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs);

  virtual const char *get_interface_introspect()
  {
    return interface_introspect;
  }

public:
  org_workrave_DebugInterface_Stub(IDBus::Ptr dbus);
  ~org_workrave_DebugInterface_Stub();


private:
  void Quit(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs);

  static const DBusMethod method_table[];
  static const char *interface_introspect;
};


org_workrave_DebugInterface *org_workrave_DebugInterface::instance(const ::workrave::dbus::IDBus::Ptr dbus)
{
  org_workrave_DebugInterface_Stub *iface = NULL;
  DBusBinding *binding = dbus->find_binding("org.workrave.DebugInterface");

  if (binding != NULL)
    {
      iface = dynamic_cast<org_workrave_DebugInterface_Stub *>(binding);
    }

  return iface;
}

org_workrave_DebugInterface_Stub::org_workrave_DebugInterface_Stub(IDBus::Ptr dbus)
  : DBusBindingGio(dbus)
{
}

org_workrave_DebugInterface_Stub::~org_workrave_DebugInterface_Stub()
{
}

void
org_workrave_DebugInterface_Stub::call(const std::string &method_name, void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs)
{
  const DBusMethod *table = method_table;
  while (table->fn != NULL)
    {
      if (method_name == table->name)
        {
          DBusMethodPointer ptr = table->fn;
          if (ptr != NULL)
            {
              (this->*ptr)(object, invocation, sender, inargs);
            }
          return;
        }
      table++;
    }

  throw DBusRemoteException()
    << message_info("Unknown method")
    << error_code_info(DBUS_ERROR_UNKNOWN_METHOD)
    << method_info(method_name)
    << interface_info("org.workrave.DebugInterface");
}


void
org_workrave_DebugInterface_Stub::Quit(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs)
{
  (void) sender;

  try
    {
      Test *dbus_object = (Test *) object;


      gsize num_in_args = g_variant_n_children(inargs);
      if (num_in_args != 0)
        {
          throw DBusRemoteException()
            << message_info("Incorrecy number of in-paraeters")
            << error_code_info(DBUS_ERROR_INVALID_ARGS)
            << method_info("Quit")
            << interface_info("org.workrave.DebugInterface");
        }


      dbus_object->quit(

      );

      GVariant *out = NULL;

      g_dbus_method_invocation_return_value(invocation, out);
    }
  catch (const DBusRemoteException &e)
    {
      e << method_info("Quit")
        << interface_info("org.workrave.DebugInterface");
      throw;
    }

}


const org_workrave_DebugInterface_Stub::DBusMethod org_workrave_DebugInterface_Stub::method_table[] = {
  { "Quit", &org_workrave_DebugInterface_Stub::Quit },
  { "", NULL }
};

const char *
org_workrave_DebugInterface_Stub::interface_introspect =
  "  <interface name=\"org.workrave.DebugInterface\">\n"
  "    <method name=\"Quit\">\n"
  "    </method>\n"
  "  </interface>\n";


#endif // defined(HAVE_TESTS)



class org_workrave_ConfigInterface_Stub : public DBusBindingGio, public org_workrave_ConfigInterface, DBusWorkrave_Marshall
{
private:
  typedef void (org_workrave_ConfigInterface_Stub::*DBusMethodPointer)(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs);

  struct DBusMethod
  {
    const string name;
    DBusMethodPointer fn;
  };

  virtual void call(const std::string &method_name, void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs);

  virtual const char *get_interface_introspect()
  {
    return interface_introspect;
  }

public:
  org_workrave_ConfigInterface_Stub(IDBus::Ptr dbus);
  ~org_workrave_ConfigInterface_Stub();


private:
  void SetString(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs);
  void SetInt(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs);
  void SetBool(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs);
  void SetDouble(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs);
  void GetString(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs);
  void GetInt(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs);
  void GetBool(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs);
  void GetDouble(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs);

  static const DBusMethod method_table[];
  static const char *interface_introspect;
};


org_workrave_ConfigInterface *org_workrave_ConfigInterface::instance(const ::workrave::dbus::IDBus::Ptr dbus)
{
  org_workrave_ConfigInterface_Stub *iface = NULL;
  DBusBinding *binding = dbus->find_binding("org.workrave.ConfigInterface");

  if (binding != NULL)
    {
      iface = dynamic_cast<org_workrave_ConfigInterface_Stub *>(binding);
    }

  return iface;
}

org_workrave_ConfigInterface_Stub::org_workrave_ConfigInterface_Stub(IDBus::Ptr dbus)
  : DBusBindingGio(dbus)
{
}

org_workrave_ConfigInterface_Stub::~org_workrave_ConfigInterface_Stub()
{
}

void
org_workrave_ConfigInterface_Stub::call(const std::string &method_name, void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs)
{
  const DBusMethod *table = method_table;
  while (table->fn != NULL)
    {
      if (method_name == table->name)
        {
          DBusMethodPointer ptr = table->fn;
          if (ptr != NULL)
            {
              (this->*ptr)(object, invocation, sender, inargs);
            }
          return;
        }
      table++;
    }

  throw DBusRemoteException()
    << message_info("Unknown method")
    << error_code_info(DBUS_ERROR_UNKNOWN_METHOD)
    << method_info(method_name)
    << interface_info("org.workrave.ConfigInterface");
}


void
org_workrave_ConfigInterface_Stub::SetString(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs)
{
  (void) sender;

  try
    {
      IConfigurator *dbus_object = (IConfigurator *) object;

      std::string p_key
      ;
      std::string p_value
      ;
      bool p_success
      ;

      gsize num_in_args = g_variant_n_children(inargs);
      if (num_in_args != 2)
        {
          throw DBusRemoteException()
            << message_info("Incorrecy number of in-paraeters")
            << error_code_info(DBUS_ERROR_INVALID_ARGS)
            << method_info("SetString")
            << interface_info("org.workrave.ConfigInterface");
        }

      GVariant *v_key = g_variant_get_child_value(inargs, 0 );
      get_string(v_key, &p_key);
      GVariant *v_value = g_variant_get_child_value(inargs, 1 );
      get_string(v_value, &p_value);

      p_success = dbus_object->set_value(

       p_key
      , p_value
      );

      GVariantBuilder builder;
      g_variant_builder_init(&builder, (GVariantType*)"(b)");

      GVariant *v_success = put_bool(&p_success);
      g_variant_builder_add_value(&builder, v_success);

      GVariant *out = g_variant_builder_end(&builder);

      g_dbus_method_invocation_return_value(invocation, out);
    }
  catch (const DBusRemoteException &e)
    {
      e << method_info("SetString")
        << interface_info("org.workrave.ConfigInterface");
      throw;
    }

}

void
org_workrave_ConfigInterface_Stub::SetInt(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs)
{
  (void) sender;

  try
    {
      IConfigurator *dbus_object = (IConfigurator *) object;

      std::string p_key
      ;
      int32_t p_value
      ;
      bool p_success
      ;

      gsize num_in_args = g_variant_n_children(inargs);
      if (num_in_args != 2)
        {
          throw DBusRemoteException()
            << message_info("Incorrecy number of in-paraeters")
            << error_code_info(DBUS_ERROR_INVALID_ARGS)
            << method_info("SetInt")
            << interface_info("org.workrave.ConfigInterface");
        }

      GVariant *v_key = g_variant_get_child_value(inargs, 0 );
      get_string(v_key, &p_key);
      GVariant *v_value = g_variant_get_child_value(inargs, 1 );
      get_int32(v_value, &p_value);

      p_success = dbus_object->set_value(

       p_key
      , p_value
      );

      GVariantBuilder builder;
      g_variant_builder_init(&builder, (GVariantType*)"(b)");

      GVariant *v_success = put_bool(&p_success);
      g_variant_builder_add_value(&builder, v_success);

      GVariant *out = g_variant_builder_end(&builder);

      g_dbus_method_invocation_return_value(invocation, out);
    }
  catch (const DBusRemoteException &e)
    {
      e << method_info("SetInt")
        << interface_info("org.workrave.ConfigInterface");
      throw;
    }

}

void
org_workrave_ConfigInterface_Stub::SetBool(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs)
{
  (void) sender;

  try
    {
      IConfigurator *dbus_object = (IConfigurator *) object;

      std::string p_key
      ;
      bool p_value
      ;
      bool p_success
      ;

      gsize num_in_args = g_variant_n_children(inargs);
      if (num_in_args != 2)
        {
          throw DBusRemoteException()
            << message_info("Incorrecy number of in-paraeters")
            << error_code_info(DBUS_ERROR_INVALID_ARGS)
            << method_info("SetBool")
            << interface_info("org.workrave.ConfigInterface");
        }

      GVariant *v_key = g_variant_get_child_value(inargs, 0 );
      get_string(v_key, &p_key);
      GVariant *v_value = g_variant_get_child_value(inargs, 1 );
      get_bool(v_value, &p_value);

      p_success = dbus_object->set_value(

       p_key
      , p_value
      );

      GVariantBuilder builder;
      g_variant_builder_init(&builder, (GVariantType*)"(b)");

      GVariant *v_success = put_bool(&p_success);
      g_variant_builder_add_value(&builder, v_success);

      GVariant *out = g_variant_builder_end(&builder);

      g_dbus_method_invocation_return_value(invocation, out);
    }
  catch (const DBusRemoteException &e)
    {
      e << method_info("SetBool")
        << interface_info("org.workrave.ConfigInterface");
      throw;
    }

}

void
org_workrave_ConfigInterface_Stub::SetDouble(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs)
{
  (void) sender;

  try
    {
      IConfigurator *dbus_object = (IConfigurator *) object;

      std::string p_key
      ;
      double p_value
      ;
      bool p_success
      ;

      gsize num_in_args = g_variant_n_children(inargs);
      if (num_in_args != 2)
        {
          throw DBusRemoteException()
            << message_info("Incorrecy number of in-paraeters")
            << error_code_info(DBUS_ERROR_INVALID_ARGS)
            << method_info("SetDouble")
            << interface_info("org.workrave.ConfigInterface");
        }

      GVariant *v_key = g_variant_get_child_value(inargs, 0 );
      get_string(v_key, &p_key);
      GVariant *v_value = g_variant_get_child_value(inargs, 1 );
      get_double(v_value, &p_value);

      p_success = dbus_object->set_value(

       p_key
      , p_value
      );

      GVariantBuilder builder;
      g_variant_builder_init(&builder, (GVariantType*)"(b)");

      GVariant *v_success = put_bool(&p_success);
      g_variant_builder_add_value(&builder, v_success);

      GVariant *out = g_variant_builder_end(&builder);

      g_dbus_method_invocation_return_value(invocation, out);
    }
  catch (const DBusRemoteException &e)
    {
      e << method_info("SetDouble")
        << interface_info("org.workrave.ConfigInterface");
      throw;
    }

}

void
org_workrave_ConfigInterface_Stub::GetString(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs)
{
  (void) sender;

  try
    {
      IConfigurator *dbus_object = (IConfigurator *) object;

      std::string p_key
      ;
      bool p_found
      ;
      std::string p_value
      ;

      gsize num_in_args = g_variant_n_children(inargs);
      if (num_in_args != 1)
        {
          throw DBusRemoteException()
            << message_info("Incorrecy number of in-paraeters")
            << error_code_info(DBUS_ERROR_INVALID_ARGS)
            << method_info("GetString")
            << interface_info("org.workrave.ConfigInterface");
        }

      GVariant *v_key = g_variant_get_child_value(inargs, 0 );
      get_string(v_key, &p_key);

      p_found = dbus_object->get_value(

       p_key
      , p_value
      );

      GVariantBuilder builder;
      g_variant_builder_init(&builder, (GVariantType*)"(bs)");

      GVariant *v_found = put_bool(&p_found);
      g_variant_builder_add_value(&builder, v_found);
      GVariant *v_value = put_string(&p_value);
      g_variant_builder_add_value(&builder, v_value);

      GVariant *out = g_variant_builder_end(&builder);

      g_dbus_method_invocation_return_value(invocation, out);
    }
  catch (const DBusRemoteException &e)
    {
      e << method_info("GetString")
        << interface_info("org.workrave.ConfigInterface");
      throw;
    }

}

void
org_workrave_ConfigInterface_Stub::GetInt(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs)
{
  (void) sender;

  try
    {
      IConfigurator *dbus_object = (IConfigurator *) object;

      std::string p_key
      ;
      int32_t p_value
      ;
      bool p_found
      ;

      gsize num_in_args = g_variant_n_children(inargs);
      if (num_in_args != 1)
        {
          throw DBusRemoteException()
            << message_info("Incorrecy number of in-paraeters")
            << error_code_info(DBUS_ERROR_INVALID_ARGS)
            << method_info("GetInt")
            << interface_info("org.workrave.ConfigInterface");
        }

      GVariant *v_key = g_variant_get_child_value(inargs, 0 );
      get_string(v_key, &p_key);

      p_found = dbus_object->get_value(

       p_key
      , p_value
      );

      GVariantBuilder builder;
      g_variant_builder_init(&builder, (GVariantType*)"(ib)");

      GVariant *v_value = put_int32(&p_value);
      g_variant_builder_add_value(&builder, v_value);
      GVariant *v_found = put_bool(&p_found);
      g_variant_builder_add_value(&builder, v_found);

      GVariant *out = g_variant_builder_end(&builder);

      g_dbus_method_invocation_return_value(invocation, out);
    }
  catch (const DBusRemoteException &e)
    {
      e << method_info("GetInt")
        << interface_info("org.workrave.ConfigInterface");
      throw;
    }

}

void
org_workrave_ConfigInterface_Stub::GetBool(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs)
{
  (void) sender;

  try
    {
      IConfigurator *dbus_object = (IConfigurator *) object;

      std::string p_key
      ;
      bool p_value
      ;
      bool p_found
      ;

      gsize num_in_args = g_variant_n_children(inargs);
      if (num_in_args != 1)
        {
          throw DBusRemoteException()
            << message_info("Incorrecy number of in-paraeters")
            << error_code_info(DBUS_ERROR_INVALID_ARGS)
            << method_info("GetBool")
            << interface_info("org.workrave.ConfigInterface");
        }

      GVariant *v_key = g_variant_get_child_value(inargs, 0 );
      get_string(v_key, &p_key);

      p_found = dbus_object->get_value(

       p_key
      , p_value
      );

      GVariantBuilder builder;
      g_variant_builder_init(&builder, (GVariantType*)"(bb)");

      GVariant *v_value = put_bool(&p_value);
      g_variant_builder_add_value(&builder, v_value);
      GVariant *v_found = put_bool(&p_found);
      g_variant_builder_add_value(&builder, v_found);

      GVariant *out = g_variant_builder_end(&builder);

      g_dbus_method_invocation_return_value(invocation, out);
    }
  catch (const DBusRemoteException &e)
    {
      e << method_info("GetBool")
        << interface_info("org.workrave.ConfigInterface");
      throw;
    }

}

void
org_workrave_ConfigInterface_Stub::GetDouble(void *object, GDBusMethodInvocation *invocation, const std::string &sender, GVariant *inargs)
{
  (void) sender;

  try
    {
      IConfigurator *dbus_object = (IConfigurator *) object;

      std::string p_key
      ;
      double p_value
      ;
      bool p_found
      ;

      gsize num_in_args = g_variant_n_children(inargs);
      if (num_in_args != 1)
        {
          throw DBusRemoteException()
            << message_info("Incorrecy number of in-paraeters")
            << error_code_info(DBUS_ERROR_INVALID_ARGS)
            << method_info("GetDouble")
            << interface_info("org.workrave.ConfigInterface");
        }

      GVariant *v_key = g_variant_get_child_value(inargs, 0 );
      get_string(v_key, &p_key);

      p_found = dbus_object->get_value(

       p_key
      , p_value
      );

      GVariantBuilder builder;
      g_variant_builder_init(&builder, (GVariantType*)"(db)");

      GVariant *v_value = put_double(&p_value);
      g_variant_builder_add_value(&builder, v_value);
      GVariant *v_found = put_bool(&p_found);
      g_variant_builder_add_value(&builder, v_found);

      GVariant *out = g_variant_builder_end(&builder);

      g_dbus_method_invocation_return_value(invocation, out);
    }
  catch (const DBusRemoteException &e)
    {
      e << method_info("GetDouble")
        << interface_info("org.workrave.ConfigInterface");
      throw;
    }

}


const org_workrave_ConfigInterface_Stub::DBusMethod org_workrave_ConfigInterface_Stub::method_table[] = {
  { "SetString", &org_workrave_ConfigInterface_Stub::SetString },
  { "SetInt", &org_workrave_ConfigInterface_Stub::SetInt },
  { "SetBool", &org_workrave_ConfigInterface_Stub::SetBool },
  { "SetDouble", &org_workrave_ConfigInterface_Stub::SetDouble },
  { "GetString", &org_workrave_ConfigInterface_Stub::GetString },
  { "GetInt", &org_workrave_ConfigInterface_Stub::GetInt },
  { "GetBool", &org_workrave_ConfigInterface_Stub::GetBool },
  { "GetDouble", &org_workrave_ConfigInterface_Stub::GetDouble },
  { "", NULL }
};

const char *
org_workrave_ConfigInterface_Stub::interface_introspect =
  "  <interface name=\"org.workrave.ConfigInterface\">\n"
  "    <method name=\"SetString\">\n"
  "      <arg type=\"s\" name=\"key\" direction=\"in\" />\n"
  "      <arg type=\"s\" name=\"value\" direction=\"in\" />\n"
  "      <arg type=\"b\" name=\"success\" direction=\"out\" />\n"
  "    </method>\n"
  "    <method name=\"SetInt\">\n"
  "      <arg type=\"s\" name=\"key\" direction=\"in\" />\n"
  "      <arg type=\"i\" name=\"value\" direction=\"in\" />\n"
  "      <arg type=\"b\" name=\"success\" direction=\"out\" />\n"
  "    </method>\n"
  "    <method name=\"SetBool\">\n"
  "      <arg type=\"s\" name=\"key\" direction=\"in\" />\n"
  "      <arg type=\"b\" name=\"value\" direction=\"in\" />\n"
  "      <arg type=\"b\" name=\"success\" direction=\"out\" />\n"
  "    </method>\n"
  "    <method name=\"SetDouble\">\n"
  "      <arg type=\"s\" name=\"key\" direction=\"in\" />\n"
  "      <arg type=\"d\" name=\"value\" direction=\"in\" />\n"
  "      <arg type=\"b\" name=\"success\" direction=\"out\" />\n"
  "    </method>\n"
  "    <method name=\"GetString\">\n"
  "      <arg type=\"s\" name=\"key\" direction=\"in\" />\n"
  "      <arg type=\"b\" name=\"found\" direction=\"out\" />\n"
  "      <arg type=\"s\" name=\"value\" direction=\"out\" />\n"
  "    </method>\n"
  "    <method name=\"GetInt\">\n"
  "      <arg type=\"s\" name=\"key\" direction=\"in\" />\n"
  "      <arg type=\"i\" name=\"value\" direction=\"out\" />\n"
  "      <arg type=\"b\" name=\"found\" direction=\"out\" />\n"
  "    </method>\n"
  "    <method name=\"GetBool\">\n"
  "      <arg type=\"s\" name=\"key\" direction=\"in\" />\n"
  "      <arg type=\"b\" name=\"value\" direction=\"out\" />\n"
  "      <arg type=\"b\" name=\"found\" direction=\"out\" />\n"
  "    </method>\n"
  "    <method name=\"GetDouble\">\n"
  "      <arg type=\"s\" name=\"key\" direction=\"in\" />\n"
  "      <arg type=\"d\" name=\"value\" direction=\"out\" />\n"
  "      <arg type=\"b\" name=\"found\" direction=\"out\" />\n"
  "    </method>\n"
  "  </interface>\n";




void init_DBusWorkrave(IDBus::Ptr dbus)
{
  dbus->register_binding("org.workrave.CoreInterface", new org_workrave_CoreInterface_Stub(dbus));
#if defined(HAVE_TESTS)
  dbus->register_binding("org.workrave.DebugInterface", new org_workrave_DebugInterface_Stub(dbus));
#endif // defined(HAVE_TESTS)
  dbus->register_binding("org.workrave.ConfigInterface", new org_workrave_ConfigInterface_Stub(dbus));
}