#ifndef DBUS_DBUSWORKRAVE_HH
#define DBUS_DBUSWORKRAVE_HH

#include "dbus/DBusBindingGio.hh"

#include "Core.hh"
#include "IConfigurator.hh"







class org_workrave_CoreInterface
{
public:
  virtual ~org_workrave_CoreInterface() {}

  static org_workrave_CoreInterface *instance(const ::workrave::dbus::IDBus::Ptr dbus);

 virtual void MicrobreakChanged(const std::string &path
            , std::string progress
        ) = 0;
 virtual void RestbreakChanged(const std::string &path
            , std::string progress
        ) = 0;
 virtual void DailylimitChanged(const std::string &path
            , std::string progress
        ) = 0;
 virtual void OperationModeChanged(const std::string &path
            , OperationMode mode
        ) = 0;
 virtual void UsageModeChanged(const std::string &path
            , UsageMode mode
        ) = 0;
 virtual void BreakPostponed(const std::string &path
            , BreakId timer_id
        ) = 0;
 virtual void BreakSkipped(const std::string &path
            , BreakId timer_id
        ) = 0;
};




#if defined(HAVE_TESTS)


class org_workrave_DebugInterface
{
public:
  virtual ~org_workrave_DebugInterface() {}

  static org_workrave_DebugInterface *instance(const ::workrave::dbus::IDBus::Ptr dbus);

};


#endif // defined(HAVE_TESTS)




class org_workrave_ConfigInterface
{
public:
  virtual ~org_workrave_ConfigInterface() {}

  static org_workrave_ConfigInterface *instance(const ::workrave::dbus::IDBus::Ptr dbus);

};




#endif // DBUS_DBUSWORKRAVE_HH