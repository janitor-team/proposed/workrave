#ifndef DBUS_DBUSGUI_HH
#define DBUS_DBUSGUI_HH

#include "dbus/DBusBindingGio.hh"

#include "Menus.hh"
#include "GenericDBusApplet.hh"







class org_workrave_ControlInterface
{
public:
  virtual ~org_workrave_ControlInterface() {}

  static org_workrave_ControlInterface *instance(const ::workrave::dbus::IDBus::Ptr dbus);

};






class org_workrave_AppletInterface
{
public:
  virtual ~org_workrave_AppletInterface() {}

  static org_workrave_AppletInterface *instance(const ::workrave::dbus::IDBus::Ptr dbus);

 virtual void TimersUpdated(const std::string &path
            , GenericDBusApplet::TimerData & micro
                , GenericDBusApplet::TimerData & rest
                , GenericDBusApplet::TimerData & daily
        ) = 0;
 virtual void MenuUpdated(const std::string &path
            , GenericDBusApplet::MenuItems & menuitems
        ) = 0;
 virtual void TrayIconUpdated(const std::string &path
            , bool enabled
        ) = 0;
};




#endif // DBUS_DBUSGUI_HH